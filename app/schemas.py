from datetime import datetime
from enum import Enum
from typing import Optional, List


from pydantic import Field, root_validator
from fastapi_camelcase import CamelModel


class SystemItemType(str, Enum):
    FILE = "FILE"
    FOLDER = "FOLDER"


class Item(CamelModel):
    id: str
    url: Optional[str] = None
    parent_id: Optional[str] = Field(None, alias="parentId")
    type: SystemItemType
    size: Optional[int] = None

    class Config:
        orm_mode = True


class SystemItemImportRequest(CamelModel):
    items: List[Item]
    update_date: datetime


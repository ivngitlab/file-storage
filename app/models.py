import enum
from sqlalchemy import Column, ForeignKey, Integer, String, DateTime, Enum
from sqlalchemy.orm import relationship, backref

from .database import Base


class ItemType(enum.Enum):
    FILE = "FILE"
    FOLDER = "FOLDER"


class Node(Base):
    __tablename__ = "node"
    id = Column(String, primary_key=True, index=True)
    parent_id = Column(String, ForeignKey("node.id"))
    children = relationship("Node", cascade="all,delete", backref=backref("parent", remote_side=[id]))
    type = Column(Enum(ItemType), nullable=False)
    url = Column(String)
    size = Column(Integer)
    date = Column(DateTime, nullable=False)

    def update_to_root(self, update_date):
        def cycle_to_root(item, _update_date):
            item.date = _update_date
            if item.parent:
                cycle_to_root(item.parent, _update_date)
            return item

        return cycle_to_root(self, update_date)

    @classmethod
    def create_or_update(cls, db, item, update_date):
        created = False

        item_db = db.query(cls).filter(cls.id == item.id).first()
        if item_db:
            item_db.parent_id = item.parent_id
            item_db.url = item.url
            item_db.size = item.size
            item_db.date = update_date
        else:
            created = True
            item_db = cls(
                id=item.id,
                parent_id=item.parent_id,
                type=item.type.name,
                url=item.url,
                size=item.size,
                date=update_date
            )
        return item_db, created

    def get_items(self):
        def cycle_sum_size(item):
            d = dict()
            d["id"] = item.id
            d["parentId"] = item.parent_id
            d["size"] = item.size
            d["type"] = item.type.name
            d["url"] = item.url
            d["date"] = item.date.strftime("%Y-%m-%dT%H:%M:%SZ")
            d["children"] = []

            if d["type"] == ItemType.FILE.name:
                d["children"] = None
                return d, d["size"]

            for child in item.children:
                data, size = cycle_sum_size(child)
                d["children"].append(data)
                if not d["size"]:
                    d["size"] = size
                else:
                    d["size"] += size

            return d, d["size"]

        return cycle_sum_size(self)[0]

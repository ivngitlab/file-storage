from datetime import datetime

from starlette.exceptions import HTTPException as StarletteHTTPException
from fastapi import Depends, FastAPI
from fastapi.exceptions import RequestValidationError
from fastapi.responses import JSONResponse
from sqlalchemy.orm import Session


from . import models
from . import schemas

from .database import SessionLocal, engine

models.Base.metadata.create_all(bind=engine)

app = FastAPI()


@app.exception_handler(StarletteHTTPException)
def http_exception_handler(request, exc):
    return JSONResponse(
        status_code=400,
        content={
            "code": 400,
            "message": "Validation Failed"
        }
    )


@app.exception_handler(RequestValidationError)
def validation_exception_handler(request, exc):
    return JSONResponse(
        status_code=400,
        content={
            "code": 400,
            "message": "Validation Failed"
        }
    )


# Dependency
def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


def validate_parent_of_file(db, item):
    if item.parent_id:
        # TODO parent may not yet be in the database, consistency is needed
        parent = db.query(models.Node).filter(models.Node.id == item.parent_id).first()
        if parent.type == schemas.SystemItemType.FILE.name and item.type.name == schemas.SystemItemType.FILE.name:
            raise StarletteHTTPException(status_code=400)


@app.post("/imports", status_code=200)
def post_imports(imports: schemas.SystemItemImportRequest, db: Session = Depends(get_db)):
    for item in imports.items:
        validate_parent_of_file(db, item)
        db_item, _ = models.Node.create_or_update(db, item, imports.update_date)
        db.add(db_item)
        db.flush()
        db_item.update_to_root(imports.update_date)
    db.commit()


@app.delete("/delete/{node_id}", status_code=200)
def delete_node(node_id: str, date: datetime, db: Session = Depends(get_db)):
    item_db = db.query(models.Node).get(node_id)
    if not item_db:
        return JSONResponse(
            status_code=400,
            content={
                "code": 400,
                "message": "Validation Failed"
            }
        )

    db.delete(item_db)
    db.commit()


@app.get("/nodes/{node_id}")
def get_node(node_id: str, db: Session = Depends(get_db)):
    item_db = db.query(models.Node).get(node_id)
    if not item_db:
        return JSONResponse(
            status_code=404,
            content={
                "code": 404,
                "message": "Item not found"
            }
        )
    items = item_db.get_items()
    return JSONResponse(status_code=200, content=items)

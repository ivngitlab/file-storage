import urllib

from fastapi.testclient import TestClient

from .main import app

client = TestClient(app)

ID = "069cb8d7-bbdd-47d3-ad8f-82ef4c269df1"
ITEM = {
    "items": [
        {
            "type": "FOLDER",
            "id": ID,
            "parentId": None
        }
    ],
    "updateDate": "2022-02-01T12:00:00Z"
}

EXPECTED_ITEMS = {
    "id": "069cb8d7-bbdd-47d3-ad8f-82ef4c269df1",
    "parentId": None,
    "size": None,
    "type":
    "FOLDER",
    "url": None,
    "date": "2022-02-01T12:00:00Z",
    "children": []
}


def test_item():
    response = client.post("/imports", json=ITEM)
    assert response.status_code == 200

    response = client.get(f"/nodes/{ID}")
    assert response.status_code == 200
    assert response.json() == EXPECTED_ITEMS

    params = urllib.parse.urlencode({
        "date": "2022-02-04T00:00:00Z"
    })
    response = client.delete(f"/delete/{ID}?{params}")
    assert response.status_code == 200

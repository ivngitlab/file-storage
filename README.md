# file-storage


## Getting started

### prepare service

env

```
export DB_NAME=project 
export DB_USER=project
export DB_PASSWORD=project
```

### run service

```
docker-compose build
docker-compose up -d
```

## developer notes

### commands

```
uvicorn app.main:app --reload --port 8000
alembic revision --autogenerate
alembic upgrade head
```

### docs

DB

`https://fastapi.tiangolo.com/tutorial/sql-databases/`
`https://docs.sqlalchemy.org/en/20/orm/self_referential.html`
